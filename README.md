# hnhance

![](https://img.shields.io/badge/written%20in-Javascript-blue)

A browser extension for the website Hacker News.

HNHance is a collection of assorted small enhancements for Hacker News (news.ycombinator.com). All features and themes can be configured and disabled by a non-intrusive settings menu. Some modifications were written by myself, some were tweaked, and some were blatantly sourced from appropriately licensed places throughout the internet.

Please feel free to modify and add features to HNHance, patches are accepted by email.

Current HNHance versions are packaged as Chrome extensions, but earlier versions were also available as a userscript for Firefox.

Tags: browser-extension

## Features

- Dim visited comment links
- Show parent hover links
- Show related 4chan /g/ links
- Show submissions link
- Confirm downvotes
- Allow collapsing threads
- Display user info on hover
- Highlight comments from submitter
- "HN-Segoe" theme
- "Modern" theme
- "GeorgifyMod" theme
- "Georgify Monokai" theme

## See Also

- HNHance on the Chrome Web Store: https://chrome.google.com/webstore/detail/hnhance/ddidnigndcepokcpocecblkfeohcedlg 
- HNHance 1.0 on userscripts.org: http://userscripts.org/scripts/show/132359 

## Changelog

2015-10-11: 1.4
- Feature: Highlight comments made by submitter
- Feature: Custom theme "HN-Segoe"
- Feature: Custom theme "Georgify Monokai" (via @PxlBuzzard CC-BY-SA 4.0)
- Enhancement: Add 300ms delay before making network request for hover user profile
- Enhancement: Replace "Collapse threads" implementation (via @niyazpk)
- Fix a compatibility issue with certain modern browsers (impacting settings menu and "Dim visited comment links" feature)
- Fix "Hover username to view profile inline" feature for most recent serverside HTML
- Fix "Show parent hover links" feature for most recent serverside HTML, and for the "Show HN" page
- Fix an issue with downvote prevention
- [⬇️ hnhance-1.4.zip](dist-archive/hnhance-1.4.zip) *(85.03 KiB)*


2013-04-09: 1.3
- Feature: Hover username to view profile inline
- [⬇️ hnhance-1.3.zip](dist-archive/hnhance-1.3.zip) *(50.73 KiB)*


2013-04-06: 1.2
- Feature: Show related discussions from 4chan /g/ (via anon)
- Feature: Show submissions link in header
- Feature: Hover username to view profile inline
- Feature: Confirm downvotes
- Feature: Collapse threads (via @drostie CC-0)
- Feature: Custom theme "Modern"
- Feature: Custom theme "GeorgifyMod" (via @tuhin et al)
- Change packaging from userscript to chrome extension
- [⬇️ hnhance-1.2.zip](dist-archive/hnhance-1.2.zip) *(50.36 KiB)*


2012-05-03: 1.0
- Feature: Show parent hover links
- Initial public release
- [⬇️ HNhance-mappy-1.0.user.js.7z](dist-archive/HNhance-mappy-1.0.user.js.7z) *(1.19 KiB)*

